/*************************************************************************/
/****  EXEMPLE 1 : DÉCLARATION DE VARIABLES  ****/
/*************************************************************************/


// JavaScript (vanilla)
// let message = "Bonjour, monde!";
// let nombre = 42;

// TypeScript
let message: string = "Bonjour, monde!";
let nombre: number = 42;

/* Ici, string et number sont des exemples de types.
 TypeScript permet de spécifier le type de données que chaque variable peut contenir.
*/



/*************************************************************************/
/****  EXEMPLE 2 : FONCTIONS  ****/
/*************************************************************************/


// JavaScript (vanilla)
// function ajouter(a, b) {
//     return a + b;
// }

// TypeScript
function ajouter(a: number, b: number): number {
    return a + b;
}

/* Dans cet exemple, la fonction ajouter accepte deux paramètres de type 
number et renvoie une valeur de type number. 
*/



/*************************************************************************/
/****  EXEMPLE 3 : INTERFACES   ****/
/*************************************************************************/


// JavaScript (vanilla)
// function afficherUtilisateur(utilisateur) {
//     console.log(utilisateur.nom);
// }

// TypeScript
interface Utilisateur {
    nom: string;
    age: number;
}

function afficherUtilisateur(utilisateur: Utilisateur): void {
    console.log(utilisateur.nom);
}

/* Les interfaces permettent de définir la structure d'un objet. 
Ici, l'interface Utilisateur spécifie que tout objet Utilisateur doit avoir une 
propriété nom de type string et une propriété age de type number.
*/



/*************************************************************************/
/****  EXEMPLE 4: CLASSES  ****/
/*************************************************************************/


// JavaScript (vanilla)
// class Animal {
//     constructor(nom) {
//         this.nom = nom;
//     }

//     faireDuBruit() {
//         console.log("Bruit indéfini");
//     }
// }

// class Chien extends Animal {
//     faireDuBruit() {
//         console.log("Woof!");
//     }
// }

// const monChien = new Chien("Buddy");
// monChien.faireDuBruit();  // Affiche "Woof!"


// TypeScript
class Animal {
    nom: string;

    constructor(nom: string) {
        this.nom = nom;
    }

    // Méthode ne renvoyant aucune valeur (void)
    faireDuBruit(): void {
        console.log("Bruit indéfini");
    }
}

class Chien extends Animal {
    faireDuBruit(): void {
        console.log("Woof!");
    }
}

const monChien = new Chien("Buddy");
monChien.faireDuBruit();  // Affiche "Woof!"

/* Les classes en TypeScript permettent d'organiser le code de manière orientée objet, 
avec la possibilité d'utiliser l'héritage et de redéfinir des méthodes.
*/

/*
En TypeScript (et en JavaScript), void est un type qui représente l'absence de tout type. 
Il est souvent utilisé comme type de retour pour indiquer qu'une fonction ne renvoie aucune valeur. 
Lorsqu'une fonction a un type de retour void, cela signifie qu'elle ne produit pas de résultat utile 
et que son résultat est essentiellement "vide" ou "indéfini".
*/



/*************************************************************************/
/****  EXEMPLE 5 : TYPES UNION ET TYPES ANY   ****/
/*************************************************************************/


// JavaScript (vanilla)
// let variable = "Bonjour";

// variable = 42;  // Pas d'erreur en JavaScript

/* En JavaScript, toutes les variables sont dynamiques et peuvent contenir n'importe 
quel type de valeur à n'importe quel moment. La flexibilité offerte par cette 
approche est une caractéristique clé de JavaScript, mais cela peut également conduire 
à des erreurs difficiles à repérer. C'est là qu'intervient la force du typage statique de TypeScript.
*/

// TypeScript
let variable: string | number = "Bonjour";

variable = 42;  // Pas d'erreur

/* Les types union permettent à une variable d'avoir plusieurs types. 
Any est un type spécial qui peut contenir n'importe quelle valeur,
 mais son utilisation est déconseillée autant que possible.
*/

