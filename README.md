
# TypeScript - Introduction

* What is TypeScript?

TypeScript is a superset of JavaScript that adds static typing features to the language. This means that you can specify the type of variables, functions, and objects, making it easier to detect errors during the development phase.
See the script to test the examples.
In summary, TypeScript provides a more robust way to develop in JavaScript by adding static typing features, which can make the code more readable, safer, easier to maintain, and enables error detection during development. Using TypeScript in large-scale projects is often recommended for these reasons.

* File Extensions

The file extension typically associated with TypeScript source files is .ts. However, if you are using files that include JSX (JavaScript XML, commonly used with React) code, you can use the .tsx extension.

.ts: This is the standard extension for TypeScript source files without JSX.

Example: myFile.ts

.tsx: This extension is used for TypeScript source files that contain JSX code (a markup syntax commonly used with React).

Example: myFile.tsx

Using the .tsx extension allows the TypeScript compiler to recognize and properly handle JSX code in the file. If you are not using JSX, you can simply use the .ts extension.



########################################## FRENCH PART ############################################


# Typescript - Introduction


##### TypeScript : Une Introduction

* Qu'est-ce que TypeScript ?

TypeScript est un sur-ensemble (superset) de JavaScript qui ajoute des fonctionnalités de typage statique au langage. Cela signifie que vous pouvez spécifier le type des variables, des fonctions et des objets, ce qui facilite la détection d'erreurs pendant la phase de développement.


Voir le script pour tester les exemples.

En résumé, TypeScript offre une manière plus robuste de développer en JavaScript en ajoutant des fonctionnalités de typage statique, ce qui peut rendre le code plus lisible, plus sûr, plus facile à maintenir et permet la détection d'erreurs pendant le développement. Utiliser TypeScript dans des projets de grande envergure est souvent recommandé pour ces raisons.

* Extension des fichiers

L'extension de fichier généralement associée aux fichiers source TypeScript est .ts. Cependant, si vous utilisez des fichiers qui incluent du code JSX (JavaScript XML, généralement utilisé avec React), vous pouvez utiliser l'extension .tsx.

.ts: C'est l'extension standard pour les fichiers source TypeScript sans JSX.

Exemple : monFichier.ts

.tsx: Cette extension est utilisée pour les fichiers source TypeScript qui contiennent du code JSX (une syntaxe de balisage utilisée fréquemment avec React).

Exemple : monFichier.tsx

L'utilisation de l'extension .tsx permet au compilateur TypeScript de reconnaître et de traiter correctement le code JSX dans le fichier. Si vous n'utilisez pas JSX, vous pouvez simplement utiliser l'extension .ts.


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
